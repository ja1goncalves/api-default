<?php
/**
 * Created by PhpStorm.
 * User: joaopaulo
 * Date: 29/05/18
 * Time: 16:21
 */

namespace App\Services;
use App\Entities\Address;
use App\Service\Traits\CrudMethods;
use App\Repositories\AddressRepositoryEloquent;
use App\Entities\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AddressService
{
    use CrudMethods;
    /**
     * @var AddressRepositoryEloquent
     */
    protected $repository;

    public function __construct(AddressRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $cep
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getUserCep($cep){
        return $this->repository->findUserByCep($cep);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function AddressesOfUserId($id){
        return $this->repository->findByField('user_id', $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function searchUserForAddress($id){
        $address = $this->repository->find($id);
        $user = User::find($address['user_id']);
        return $user;
    }

}