<?php

namespace App\Services;

use App\Repositories\UserRepositoryEloquent;
use App\Service\Traits\CrudMethods;

/**
 * Class BankAccountService
 * @package App\Services
 */
class UserService
{
    use CrudMethods;
    /**
     * @var UserRepositoryEloquent
     */
    protected $repository;

    public function __construct(UserRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    public function usersCep($cep){
        /*
        try {
            return $this->repository->userCep($cep);
        } catch (RepositoryException $e) {
            return response()->json(['nenhum user encontrado'], 404);
        }*/
        return $this->repository->findByField('cep', $cep);
    }

}