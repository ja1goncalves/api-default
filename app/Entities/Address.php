<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Address.
 *
 * @package namespace App\Entities;
 */
class Address extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'cep', 'street', 'number', 'city', 'UF', 'country', 'complements', 'user_id'];

    protected $dates = ['create_id', 'deleted_id', 'updated_id'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }


}
