<?php

namespace App\Repositories;

use App\Entities\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AddressRepository;
use App\Entities\Address;
use App\Validators\AddressValidator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AddressRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AddressRepositoryEloquent extends AppRepository implements AddressRepository
{

    protected $fieldSearchable = [
        'id'        => 'like',
        'cep'       => 'like',
        'street'    => 'like',
    ];

    protected $fieldsRules = [
        'id'        => ['numeric', 'max:2147483647'],
        'cep'       => ['string', 'max:9'],
        'street'    => ['max:20'],
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Address::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AddressValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $cep
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function findUserByCep($cep){
        $this->applyCriteria();
        $this->applyScope();

        $addresses = $this->model()::all();
        $users = array();

        foreach ($addresses as $address){
            if($address['cep'] == $cep)
                $users[] = User::find($address['user_id']);
        }

        $this->resetModel();
        $this->resetScope();

        return $this->parserResult(response()->json($users));
    }

}
