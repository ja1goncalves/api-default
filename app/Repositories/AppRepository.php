<?php
/**
 * Created by PhpStorm.
 * User: joaopaulo
 * Date: 01/06/18
 * Time: 14:29
 */

namespace App\Repositories;


use Prettus\Repository\Eloquent\BaseRepository;

class AppRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldsRules = [];
    /**
     * Get Fields Types
     *
     * @return array
     */
    public function getFieldsRules()
    {
        return $this->fieldsRules;
    }

    /**
     * Boot up the repository, pushing criteria
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(AppRequestCriteria::class));
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        // TODO: Implement model() method.
    }
}