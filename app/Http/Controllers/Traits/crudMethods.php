<?php
namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
//use App\Validators\AddressValidator;
use App\Services\AddressService;

/**
 * Class CrudMethods
 * @package app\Http\Controllers\Traits
 */
trait CrudMethods
{
    /** @var  ValidatorInterface $validator */
    protected $validator;

    /** @var AddressService $service */
    protected $service;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //$limit = $request->query->get('limit', 15);
        return response()->json($this->service->all());
    }

    /**
     * Display the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return response()->json($this->service->find($id));
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        if($request['email']){
            if(!$this->isMail($request['email']))
                return response()->json(['message' => 'Email inválido']);
        }
        if($request['cep']){
            if(!$this->isCep($request['cep']))
                return response()->json(['message' => 'CEP inválido']);
        }
        $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
        return $this->service->create($request->all(), true);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Request $request, int $id)
    {
        if($request['email']){
            if(!$this->isMail($request['email']))
                return response()->json(['message' => 'Email inválido']);
        }
        if($request['cep']){
            if(!$this->isCep($request['cep']))
                return response()->json(['message' => 'CEP inválido']);
        }
        $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
        return $this->service->update($request->all(), $id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id){
        return $this->service->delete($id);
    }

    private function isMail(string $mail){
        return preg_match("#^([\w\.-]+)\@([\w\.-]+)+\.([a-z]{2,6})$#",$mail);
    }
    private function isCep(string $cep){
        $cep = trim($cep);
        // implementar verificação na APICEP
        return preg_match("#^[0-9]{5}-[0-9]{3}$#", $cep);
    }
}