<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\AddressService;
use App\Validators\AddressValidator;
use App\Http\Controllers\Traits\CrudMethods;

/**
 * Class AddressesController.
 *
 * @package namespace App\Http\Controllers;
 */
class AddressesController extends Controller
{
    use CrudMethods;
    /**
     * @var AddressService
     */
    protected $service;
    /**
     * @var AddressValidator
     */
    protected $validator;
    /**
     * UsersController constructor.
     * @param AddressService $service
     * @param AddressValidator $validator
     */
    public function __construct(AddressService $service, AddressValidator $validator)
    {
        $this->service = $service;
        $this->validator  = $validator;
    }

    /**
     * @param string $cep
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getUserCep(string $cep){
        return response()->json($this->service->getUserCep($cep));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function AddressesOfUserId(int $id){
        $address = $this->service->AddressesOfUserId($id);

        if(empty($address))
            return response()->json(['message' => 'Usuario inextistente!'], 404);

        return response()->json($address, 200);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUserForAddress(int $id){
        return response()->json($this->service->searchUserForAddress($id));
    }
}
