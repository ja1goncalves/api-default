<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterByAddressCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
class FilterByAddressCriteriaCriteria implements CriteriaInterface
{
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $street = $this->request->query->get('street');
        $cep = $this->request->query->get('cep');
        $number = $this->request->query->get('number');
        if (isset($cep)) {
            $model = $model->where('cep', $cep);
        }
        if (is_numeric($number)) {
            $model = $model->where('number', $number);
        }
        if (isset($street)) {
            $model = $model->where('street', $street);
        }
        return $model;
    }
}

