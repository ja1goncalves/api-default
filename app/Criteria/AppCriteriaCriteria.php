<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class AppCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
abstract class AppCriteriaCriteria implements CriteriaInterface
{
    /** @var \Illuminate\Http\Request */
    protected $request;

    /**
     * FilterByStatusCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
