<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FilterBySearchCriteria
 * @package App\Criterias
 */
class FilterByUserCriteriaCriteria implements CriteriaInterface
{
    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $name    = $this->request->query->get('name');
        $email = $this->request->query->get('email');
        if (isset($name)) {
            $model = $model->where('name', $name);
        }
        if (isset($email)) {
            $model = $model->where('email', $email);
        }
        return $model;
    }
}
