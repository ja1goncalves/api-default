<?php

use Illuminate\Http\Request;

//Rotas fechadas
Route::group(['middleware' => 'auth:api'], function () {
    //USERS
    Route::resource('/users', 'UsersController', ['except' => ['isCep', 'isMail']]);
    Route::get('/cep/{cep}', 'UsersController@showUserCep');

    //ADDRESSES
    Route::resource('/addresses', 'AddressesController', ['except' => []]);
    Route::get('/addresses/user/{cep}', 'AddressesController@getUserCep');                      // USERS of cep
    Route::get('/address/users/{id}', 'AddressesController@AddressesOfUserId');                   //Address of a user with id
    Route::get('/user/address/{id}', 'AddressesController@searchUserForAddress');               //User that has a address with id
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/