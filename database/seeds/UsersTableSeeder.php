<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'João Paulo',
            'email' => 'joaosilva@mangue3.com',
            'password' => bcrypt('12345'),
        ]);
    }
}
