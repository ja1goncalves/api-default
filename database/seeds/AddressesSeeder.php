<?php

use Illuminate\Database\Seeder;

class AddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->generateCep();
        DB::table('addresses')->insert([
            'cep' => '55784-123',
            'street' => 'Rua João Ivo da Silva',
            'UF' => 'PE',
            'city' => 'Recife',
            'contry' => 'Brasil',
            'number' => 65,
            'complements' => 'apto 01',
            'user_id' => 3,
        ]);
    }

    public function generateCep(){
        $users = App\User::all();
        foreach ($users as $user){
            DB::table('addresses')->insert([
                'cep' => $user['cep'],
                'street' => 'Rua '.$user['name'],
                'UF' => 'PE',
                'city' => 'Recife',
                'contry' => 'Brasil',
                'number' => $user['id']+10,
                'complements' => 'apto 01',
                'user_id' => $user['id'],
            ]);
        }
    }
}
